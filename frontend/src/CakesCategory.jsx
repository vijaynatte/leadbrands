import React from "react";
import { Link } from "react-router-dom";
import Footer from "./components/Footer";

const CakesCategory = () => {
  return (
    <div
      className="home-container"
      style={{
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "5%",
        backgroundColor: "#ededed",
      }}
    >
      <h2>Product list</h2>
      {/* // regular product */}
      <div className="category_main">
        <Link to="/regular_cake">
        <div class="p-card-container mx-5">
          <div class="p-card">
            <div class="p-front-content">
              <img src="images/butterscotch.jpg" alt="not found" />
            </div>
            <div class="p-content">
              <p class="p-heading">Regular_cakes</p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipii voluptas ten
                mollitia pariatur odit, ab minus ratione adipisci accusamus vel
                est excepturi laboriosam magnam necessitatibus dignissimos
                molestias.
              </p>
            </div>
          </div>
        </div>
        </Link>
        <Link to="/premium_cake">
        <div class="p-card-container">
          <div class="p-card">
            <div class="p-front-content">
              <img src="images/newyork2.jpg" alt="not found" />
            </div>
            <div class="p-content">
              <p class="p-heading">Premium_cakes</p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipii voluptas ten
                mollitia pariatur odit, ab minus ratione adipisci accusamus vel
                est excepturi laboriosam magnam necessitatibus dignissimos
                molestias.
              </p>
            </div>
            <span></span>
          </div>
        </div>
        </Link>
      </div>
      <Footer />
    </div>
  );
};

export default CakesCategory;
