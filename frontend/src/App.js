import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// import NavBar from "./components/NavBar";
import Home from "./components/Home";
import Cart from "./components/Cart";
import NotFound from "./components/NotFound";
import { ToastContainer } from "react-toastify";
// import Hero from "./components/Hero/Hero";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import CheckoutSuccess from "./components/CheckoutSuccess";
import ProductDescription from "./components/ProductDescription";

import "mdb-react-ui-kit/dist/css/mdb.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import AboutUs from "./components/AboutUs";
import Headertsection from "./components/Headertsection";
import Main from "./components/Main";
import Cakes from "./components/Cakes";
import Address from "./components/Address";
import CakesCategory from "./CakesCategory";

function App() {
  return (
    <div>
      <BrowserRouter>
        <ToastContainer />
        {/* <NavBaar/> */}
        {/* <NavBar /> */}
        {/* <Hero /> */}
        <Headertsection />
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/product" element={<Home />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/checkout-success" element={<CheckoutSuccess />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/productitem" element={<ProductDescription />} />
          <Route path="/aboutus" element={<AboutUs />} />
          <Route path="/address" element={<Address />} />
          <Route path="/cake" element={<CakesCategory />} />
          <Route path="/regular_cake" element={<Cakes />} />
          <Route path="/premium_cake" element={<Cakes />} />
          <Route path="/dry_cake" element={<Cakes />} />
          <Route path="/breads" element={<Cakes />} />
          <Route path="/cookies" element={<Cakes />} />
          <Route path="/rusk" element={<Cakes />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
