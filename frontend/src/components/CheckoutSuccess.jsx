import React from "react";
import { Link } from "react-router-dom";

const CheckoutSuccess = () => {
  return (
    <div className="checkout">
    <div className="card">
      <Link to="/">
      <button type="button" className="dismiss">
        ×
      </button>
      </Link>
      
      <div className="header">
        <div className="image">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <g stroke-width="0" id="SVGRepo_bgCarrier"></g>
            <g
              strokeLinejoin="round"
              strokeLinecap="round"
              id="SVGRepo_tracerCarrier"
            ></g>
            <g id="SVGRepo_iconCarrier">
              {" "}
              <path
                strokeLinejoin="round"
                strokeLinecap="round"
                strokeWidth="1.5"
                stroke="#000000"
                d="M20 7L9.00004 18L3.99994 13"
              ></path>{" "}
            </g>
          </svg>
        </div>
        <div className="content">
          <span className="title">Order confirmed</span>
          <p className="message">
            Thank you for your purchase. you package will be delivered within 1
            days of your purchase
          </p>
        </div>
        <div className="actions">
          <button type="button" className="history">
            History
          </button>
          <button type="button" className="track">
            Track my package
          </button>
        </div>
      </div>
    </div>
    </div>
  );
};

export default CheckoutSuccess;
