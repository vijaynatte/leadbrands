import React from "react";
import { useState } from "react";
import ReactImageMagnify from "react-image-magnify";
import Footer from "./Footer";
import { BiRupee } from "react-icons/bi";
import { GrStar } from "react-icons/gr";
// import Headertsection from "./Headertsection";
import { Link } from "react-router-dom";
// import { data } from "./Data";

const images = [
  "images/image.jpeg",
  "images/image1.png",
  "images/image3.webp",
  "images/image4.jpg",
  "images/image2.jpeg",
];

const ProductDescription = () => {
  const [img, setImage] = useState(images[0]);
  const [price, setPrice] = useState(399);
  const [displayPrice, setDisplayPrice] = useState(price);
  let [imageIndex, setImageIndex] = useState(0);

  const handleHover = (image, i) => {
    setImage(image);
    setImageIndex(i);
  };
  return (
    <div>
      <div className="body">
        <div className="container-mag">
          <div className="left">
            <div className="left_1">
              {images.map((image, i) => (
                <div
                  key={i}
                  className={
                    i == imageIndex ? "image_wrap active" : "image_wrap"
                  }
                  onMouseOver={() => handleHover(image, i)}
                >
                  <img src={image} alt="no data found" />
                </div>
              ))}
            </div>
            <div className="left_2">
              <ReactImageMagnify
                {...{
                  smallImage: {
                    alt: "Wristwatch by Ted Baker London",
                    isFluidWidth: true,
                    src: img,
                    className: {},
                    width: 400,
                    height: 400,
                  },
                  largeImage: {
                    src: img,
                    width: 1200,
                    height: 1800,
                  },
                  shouldUsePositiveSpaceLens: true,
                }}
              />
            </div>
          </div>

          <div className="right">
            <h3>Cake</h3>
            <div className="ratings">
              <div>
                4.4
                <GrStar />
              </div>
              <div>966 ratings & 128 reviews</div>
              <div>flipkart emoji</div>
            </div>
            <div className="product_price">
              <span>
                {" "}
                <BiRupee />
                {displayPrice}
              </span>
              <strike>
                {" "}
                <BiRupee />
                {parseInt(displayPrice) + 200}
              </strike>
            </div>
            <div className="cake_photos">
              <span>pick an upgrade</span>
              <select
                onChange={(event) => {
                  const selectedValue = parseInt(event.target.value);
                  setDisplayPrice(selectedValue * price);
                }}
              >
                {Array.from({ length: 50 }, (_, i) => (
                  <option key={i} value={i + 1}>
                    {i + 1} Kg
                  </option>
                ))}
              </select>

              <div className="cake_images">
                <div
                  className="sub_cake"
                  onClick={() => {
                    setDisplayPrice(1 * price);
                  }}
                >
                  <img src={images[0]} alt="nothing" width={50} height={50} />
                  <span>1kg</span>
                </div>
                <div
                  className="sub_cake"
                  onClick={() => {
                    setDisplayPrice(2 * price);
                  }}
                >
                  <img src={images[0]} alt="nothing" width={50} height={50} />
                  <span>2kg</span>
                </div>
                <div
                  className="sub_cake"
                  onClick={() => {
                    setDisplayPrice(5 * price);
                  }}
                >
                  <img src={images[0]} alt="nothing" width={50} height={50} />
                  <span>5kg</span>
                </div>
              </div>
              <div className="form_page">
                <form action="">
                  <div className="text_date">
                    {/* <input type="text" placeholder="Enter pincode, Location" /> */}
                    {/* <input type="date" placeholder="select Delivery date" /> */}
                    <select id="slot booking" className="slot ">
                      <option>Available Slots</option>
                      <option value="03:00pm To 06:00pm">
                        03:00pm To 06:00pm
                      </option>
                    </select>
                  </div>

                  <div className="text_radio">
                    <div className="rad mt-2">
                      <input
                        type="radio"
                        name="type"
                        onClick={() => {
                          setDisplayPrice(price);
                        }}
                      />
                      <label htmlFor="" className="me-2">
                        Regular
                      </label>
                      <input
                        type="radio"
                        name="type"
                        onClick={() => {
                          setDisplayPrice(parseInt(displayPrice) + 200);
                        }}
                      />
                      <label htmlFor="">Eggless</label>
                    </div>
                    <br />
                  </div>
                  <div className="mess mt-2 ">
                    <input type="text" placeholder="Message on cake" />
                  </div>
                  <div className="buttons">
                    <Link to="/cart">
                      <button>Add to cart</button>
                    </Link>

                    {/* <button>Buy Now</button> */}
                  </div>
                </form>
                <div className="description">
                  <h2>Description</h2>
                  <ul>
                    <li>Cake Flavour - Chocolate</li>
                    <li>Type of Cake - Cream</li>
                    <li>Weight - Half Kg</li>
                    <li>Shape - Round</li>
                    <li>Serves: 4-6 People</li>
                    <li>Size 6 inches in Diameter</li>
                  </ul>
                </div>
                <div className="description">
                  <h2>Delivery Information</h2>
                  <ul>
                    <li>
                      The cake stand, cutlery & accessories used in the image
                      are only for representation purpose. They are not
                      delivered with the cake. This cake is hand delivered in a
                      good quality cardboard box. Country of Origin: India
                    </li>
                    <li>
                      Every cake we offer is handcrafted and since each chef has
                      his/her own way of baking and designing a cake, there
                      might be slight variation in the product in terms of
                      design and shape.
                    </li>
                    {/* <li>
                      The chosen delivery time is an estimate and depends on the
                      availability of the product and the destination to which
                      you want the product to be delivered.
                    </li> */}
                    <li>
                      Since cakes are perishable in nature, we attempt delivery
                      of your order only once. The delivery cannot be redirected
                      to any other address.
                    </li>
                    {/* <li>
                      Occasionally, substitutions of flavours/designs is
                      necessary due to temporary and/or regional
                      unavailability issues.
                    </li> */}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* description */}
      </div>
      <Footer />
    </div>
  );
};

export default ProductDescription;
