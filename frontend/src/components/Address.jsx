import React from "react";
import { useSelector } from "react-redux";
import PayButton from "./PayButton";
import { useNavigate } from "react-router-dom";

const Address = () => {
  const cart = useSelector((state) => state.cart);
  const auth = useSelector((state) => state.auth);

  const navigate = useNavigate();
  return (
    <div>
      <div
        className="container py-5 "
        style={{ position: "absolute", top: "180px", padding: "5vw" }}
      >
        <div className="row d-flex justify-content-center align-items-center">
          <div className="col">
            <div className=" my-4 shadow-3">
              <div className="row g-0">
                <div className="col-xl-6 d-xl-block bg-image">
                  <img
                    src="https://mdbcdn.b-cdn.net/img/Others/extended-example/delivery.webp"
                    alt="Samplephoto"
                    className="img-fluid"
                  />
                  <div
                    className="mask"
                    style={{ backgroundColor: "rgba(0, 0, 0, 0.6)" }}
                  >
                    <div className=" justify-content-center align-items-center h-100">
                      <div
                        className=" text-center"
                        style={{ marginTop: "220px" }}
                      >
                        <i className="fas fa-truck text-white fa-3x"></i>
                        <p className="text-white title-style">
                          Delivering happiness to your home
                        </p>
                        <p className="text-white mb-0"></p>

                        <figure className="text-center mb-0">
                          <blockquote className="blockquote text-white">
                            <p className="pb-3">
                              <i
                                className="fas fa-quote-left fa-xs text-primary"
                                style={{ color: "hsl(210, 100%, 50%)" }}
                              ></i>
                              <span className="lead font-italic">
                                Everything at your doorstep.
                              </span>
                              <i
                                className="fas fa-quote-right fa-xs text-primary"
                                style={{ color: "hsl(210, 100%, 50%)" }}
                              ></i>
                            </p>
                          </blockquote>
                        </figure>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6">
                  <div className="card-body p-md-5 text-black">
                    <h3 className="mb-4 text-uppercase">Delivery Info</h3>

                    <div className="row">
                      <div className="col-md-6 mb-4">
                        <div className="form-outline">
                          <input
                            type="text"
                            id="form3Example1m"
                            className="form-control form-control-lg border-bottom"
                          />
                          <label className="form-label" for="form3Example1m">
                            First name
                          </label>
                        </div>
                      </div>
                      <div className="col-md-6 mb-4">
                        <div className="form-outline">
                          <input
                            type="text"
                            id="form3Example1n"
                            className="form-control form-control-lg border-bottom"
                          />
                          <label className="form-label" for="form3Example1n">
                            Last name
                          </label>
                        </div>
                      </div>
                    </div>

                    <div className="form-outline mb-4">
                      <input
                        type="text"
                        id="form3Example8"
                        className="form-control form-control-lg border-bottom"
                      />
                      <label className="form-label" for="form3Example8">
                        Address
                      </label>
                    </div>
                    <div className="form-outline mb-4">
                      <input
                        type="text"
                        id="form3Example8"
                        className="form-control form-control-lg border-bottom"
                      />
                      <label className="form-label" for="form3Example8">
                        Landmark
                      </label>
                    </div>

                    <div className="row">
                      <div className="form-outline mb-4">
                      <input
                        type="text"
                        id="form3Example8"
                        className="form-control form-control-lg border-bottom"
                      />
                      <label className="form-label" for="form3Example8">
                        City
                      </label>
                      </div>
                      <div className="form-outline mb-4">
                      <input
                        type="text"
                        id="form3Example8"
                        className="form-control form-control-lg border-bottom"
                      />
                      <label className="form-label" for="form3Example8">
                        State
                      </label>
                      </div>
                    </div>

                    <div className="form-outline mb-4">
                      <input
                        type="tel"
                        id="form3Example3"
                        className="form-control form-control-lg border-bottom"
                      />
                      <label className="form-label" for="form3Example3">
                        Pincode
                      </label>
                    </div>

                    <div className="form-outline mb-4">
                      <input
                        type="text"
                        id="form3Example2"
                        className="form-control form-control-lg border-bottom"
                      />
                      <label className="form-label" for="form3Example2">
                        Email
                      </label>
                    </div>

                    <div class="d-flex justify-content-end pt-3">
                      {auth._id ? (
                        <PayButton cartItems={cart.cartItems} />
                      ) : (
                        <button
                          className="cart-login "
                          onClick={() => navigate("/login")}
                        >
                          Login to checkout
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            
        </div>
      </div>
    </div>
  );
};

export default Address;
