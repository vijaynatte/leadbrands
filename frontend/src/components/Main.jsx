import React from "react";
import { useState } from "react";
import Carousel from "react-bootstrap/Carousel";

import Footer from "./Footer";
import { Link } from "react-router-dom";

const Main = () => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };
  return (
    <div>
      <div
        className="mt-5"
        style={{
          position: "absolute",
          top: "100px",
        }}
      >
        {/* <video width="100%" height="70%" controls autoPlay loop playsInline muted>
          <source src="videos123/v1.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video> */}
        <Carousel
          slide={false}
          className="container-fluid"
          activeIndex={index}
          onSelect={handleSelect}
          style={{ padding: "0 5vw" }}
        >
          <Carousel.Item style={{ border: "none" }}>
            <video
              controls
              autoPlay
              loop
              playsInline
              muted
              className="d-block "
              style={{ width: "1300px", height: "70vh" }}
            >
              <source src="videos123/v1.mp4" type="video/mp4" />
              Your browser does not support the video tag.
            </video>
            <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item style={{ border: "none" }}>
            {/* <img
              className="d-block "
              style={{ width: "1300px", height: "70vh" }}
              src="images/a.jpg"
              alt="Second slide"
            /> */}
            <video
              controls
              autoPlay
              loop
              playsInline
              muted
              className="d-block "
              style={{ width: "1300px", height: "70vh" }}
            >
              <source src="video1/faheem.mp4" type="video/mp4" />
              Your browser does not support the video tag.
            </video>

            <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item style={{ border: "none" }}>
            <video
              controls
              autoPlay
              loop
              playsInline
              muted
              className="d-block "
              style={{ width: "1300px", height: "70vh" }}
            >
              <source src="video1/modelfaheem.mp4" type="video/mp4" />
              Your browser does not support the video tag.
            </video>

            <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>
                Praesent commodo cursus magna, vel scelerisque nisl consectetur.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </div>
      {/* main categories */}
      <div
        className="container-fluid main-section"
        style={{ position: "absolute", top: "700px" }}
      >
        <div className="m-card">
          <Link to="/cake">
            <div className="m-card-image">
              <img src="images/image22.png" alt="iudhcvciueygv" />
            </div>
            <div className="m-card-description">
              <p className="m-text-title"> Cakes</p>
              <p className="m-text-body">
                Cool Cakes for BirthDays and Occassions
              </p>
            </div>
          </Link>
        </div>
        <Link to="/breads">
          <div className="m-card">
            <div className="m-card-image">
              <img src="images/breads3.jpeg" alt="iudhcvciueygv" />
            </div>
            <div className="m-card-description">
              <p className="m-text-title"> Breads</p>
              <p className="m-text-body">
                Good bread is the most fundamentally satisfying of all foods;
                and good bread with fresh butter, the greatest of feasts.
              </p>
            </div>
          </div>
        </Link>
        <Link to="/dry_cake">
          <div className="m-card">
            <div className="m-card-image">
              <img src="images/cakes1.jpeg" alt="iudhcvciueygv" />
            </div>
            <div className="m-card-description">
              <p className="m-text-title"> Dry Cake</p>
              <p className="m-text-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor.
              </p>
            </div>
          </div>
        </Link>
        <Link to="/cookies">
          <div className="m-card">
            <div className="m-card-image">
              <img src="images/mac7.jpeg" alt="iudhcvciueygv" />
            </div>
            <div className="m-card-description">
              <p className="m-text-title"> Cookies</p>
              <p className="m-text-body">
                It’s much harder to be sad while eating a cookie.
              </p>
            </div>
          </div>
        </Link>
      </div>
      <div className="main-footer">
        <Footer />
      </div>
    </div>
  );
};

export default Main;
