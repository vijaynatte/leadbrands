import React from "react";
// import { useGetAllProductsQuery } from "../features/productsApi";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cartSlice";
import { Link, useNavigate } from "react-router-dom";
import { BiRupee } from "react-icons/bi";
import Footer from "./Footer";
import data from "./Data";

const Cakes = () => {
  // refering from url  category wise
  let splitUrl = window.location.href.split("/");
  let cat = splitUrl[3];

  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleAddToCart = (item) => {
    dispatch(addToCart(item));
    navigate("/cart");
  };

  let filteredData = data.filter((item, key) => {
    console.log("category to fetch", cat);
    console.log(item.category);
    return item.category === cat;
  });
  // filtering form back end
  // let filteredItems = data.filter((item, key) => {
  //   return item.category === cat;
  // });

  return (
    <div>
      <div
        className="home-container"
        style={{ marginLeft:"auto", marginRight:"auto",marginTop:"5%",backgroundColor:"#ededed"}}
      >
        <>
          <h2>Product list</h2>
          <div className="products">
            {filteredData.map((item, key) => (
              <div key={key} className="product">
                <Link to={"/productitem"}>
                  <h3>{item.name}</h3>
                  <img src={item.image} alt={item.name} />
                </Link>
                <div className="quantity">
                  <span>{item.quantity}</span>
                  <div className="details">
                  <span>{item.desc}</span>
                  </div>
                  <span className="price">
                    <BiRupee />
                    {item.price}
                  </span>
                </div>
                <button onClick={() => handleAddToCart(item)}>
                  Add to cart
                </button>
              </div>
            ))}
          </div>
        </>
        <div className="w-100">
          
        </div>
      </div>
      <Footer/>
    </div>
    
  );
};

export default Cakes;
