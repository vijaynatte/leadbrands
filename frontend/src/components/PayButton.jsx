import axios from "axios";
import { useSelector } from "react-redux";
import { url } from "../features/api";

import React from "react";

const PayButton = ({ cartItems }) => {
  const user = useSelector((state) => state.auth);

  const handleCheckout = () => {
    axios
      .post(`${url}/stripe/create-checkout-session`, {
        cartItems,
        userId: user._id,
      })
      .then((res) => {
        if (res.data.url) {
          window.location.href = res.data.url;
        }
      })
      .catch((error) => console.log(error.message));
  };
  return (
    <>
      <button onClick={() => handleCheckout()} className="btn btn-success">Checkout</button>
    </>
  );
};

export default PayButton;
