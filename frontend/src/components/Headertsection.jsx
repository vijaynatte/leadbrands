import React from "react";
import NavDropdown from "react-bootstrap/NavDropdown";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const Headertsection = () => {
  const { cartTotalQuantity } = useSelector((state) => state.cart);
  return (
    <>
      <header style={{ width: "100%", zIndex: "10000" }}>
        {/* <!-- Jumbotron --> */}
        <div
          className="p-3 text-center  border-bottom "
          style={{
            background: "linear-gradient(45deg, #E7717D,#C2CAD0)",
          }}
        >
          <div className="nav-container">
            <div className="row gy-3">
              {/* <!-- Left elements --> */}
              <div className="col-lg-2 col-sm-4 col-4">
                <Link to="/">
                  <img
                    src="images/brimg.png"
                    alt="nooooo"
                    height="45"
                    className="float-start"
                  />
                  <span style={{ color: "red", fontSize: "25px" }}>
                    Health Cart
                  </span>
                </Link>
              </div>
              {/* <!-- Left elements/ --> */}

              {/* <!-- Center elements --> */}
              <div className="order-lg-last col-lg-5 col-sm-8 col-8">
                <div className="d-flex float-end">
                  <a
                    href="/"
                    className="me-1  rounded py-1 px-3 nav-link d-flex align-items-center"
                    style={{ border: "1px solid black" }}
                    target="_blank"
                  >
                    {" "}
                    <i className="fas fa-user-alt m-1 me-md-2"></i>
                    <p className="d-none d-md-block mb-0">Sign in</p>{" "}
                  </a>
                  <a
                    href="/"
                    className="me-1  rounded py-1 px-3 nav-link d-flex align-items-center"
                    style={{ border: "1px solid black" }}
                    target="_blank"
                  >
                    {" "}
                    <i className="fas fa-heart m-1 me-md-2"></i>
                    <p className="d-none d-md-block mb-0">Wishlist</p>{" "}
                  </a>

                  <Link to="/cart">
                    {" "}
                    <i className="fas fa-shopping-cart m-1 me-md-2"></i>
                    <span className="bag-quantity">
                      <span>{cartTotalQuantity}</span>
                    </span>
                    <p className="d-none d-md-block mb-0">My cart</p>{" "}
                  </Link>
                </div>
              </div>
              {/* <!-- Center elements --> */}

              {/* <!-- Right elements --> */}
              <div className="col-lg-5 col-md-12 col-12">
                <div className="input-group float-center">
                  <div className="form-outline">
                    <input
                      type="search"
                      id="form1"
                      className="form-control"
                      style={{ border: "1px solid black" }}
                    />
                    <label className="form-label" for="form1">
                      Search
                    </label>
                  </div>
                  <button type="button" className="btn btn-primary shadow-0">
                    <i className="fas fa-search"></i>
                  </button>
                </div>
              </div>
              {/* <!-- Right elements --> */}
            </div>
          </div>
        </div>
        {/* <!-- Jumbotron --> */}
      </header>

      {/* <!-- Navbar --> */}
      <div
        className="navbar_container"
        style={{ position: "absolute", top: "76px", width: "100%" }}
      >
        <nav
          className="navbar navbar-expand-lg navbar-light "
          style={{ backgroundColor: "#fff" }}
        >
          {/* <!-- Container wrapper --> */}
          <div className="container justify-content-center justify-content-md-between">
            {/* <!-- Toggle button --> */}
            <button
              className="navbar-toggler border text-dark py-2"
              type="button"
              data-mdb-toggle="collapse"
              data-mdb-target="#navbarLeftAlignExample"
              aria-controls="navbarLeftAlignExample"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i className="fas fa-bars"></i>
            </button>

            {/* <!-- Collapsible wrapper --> */}
            <div
              className="collapse navbar-collapse"
              id="navbarLeftAlignExample"
            >
              {/* <!-- Left links --> */}
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <a
                    className="nav-link text-dark"
                    aria-current="page"
                    href="/"
                  >
                    Home
                  </a>
                </li>
                <Nav style={{ color: "black" }}>
                  <NavDropdown
                    id="nav-dropdown-dark-example"
                    title="Categories"
                    menuVariant="dark"
                  >
                    <Link to="/cake">
                      <NavDropdown.Item href="#action/3.1">
                        Cakes
                      </NavDropdown.Item>
                    </Link>
                    <Link to="/breads">
                      <NavDropdown.Item href="#action/3.2">
                        Breads
                      </NavDropdown.Item>
                    </Link>
                    <Link to="/dry_cake">
                      <NavDropdown.Item href="#action/3.3">
                        Dry Cakes
                      </NavDropdown.Item>
                    </Link>
                    <Link to="/cookies">
                      <NavDropdown.Item href="#action/3.4">
                        Biscuits
                      </NavDropdown.Item>
                    </Link>
                  </NavDropdown>
                </Nav>
                <Nav>
                  <NavDropdown
                    id="nav-dropdown-dark-example"
                    title="Offers"
                    menuVariant="dark"
                  >
                    <NavDropdown.Item href="#action/3.1">
                      {" "}
                      On Cakes
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">
                      {" "}
                      On Breads
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">
                      On Rusk
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.4">
                      On Biscuits
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav>
                <Nav>
                  <NavDropdown
                    id="nav-dropdown-dark-example"
                    title="Discounts"
                    menuVariant="dark"
                  >
                    <NavDropdown.Item href="#action/3.1">
                      On Cakes
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">
                      On Breads
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">
                      {" "}
                      On Rusk
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.4">
                      On Biscuits
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav>
              </ul>
              {/* <!-- Left links --> */}
            </div>
          </div>
          {/* <!-- Container wrapper --> */}
        </nav>
      </div>
      {/* <!-- Navbar --> */}
    </>
  );
};

export default Headertsection;
