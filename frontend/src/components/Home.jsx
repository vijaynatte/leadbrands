import React from "react";
import { useGetAllProductsQuery } from "../features/productsApi";
import { useDispatch } from "react-redux";
import { addToCart } from "../features/cartSlice";
import { Link, useNavigate } from "react-router-dom";
import { BiRupee} from "react-icons/bi";
import Footer from "./Footer";

const Home = () => {

  const dispatch = useDispatch()

  const navigate = useNavigate();
  
  const handleAddToCart = (product) =>{
    dispatch(addToCart(product))
    navigate("/cart")
  }
  
  let { data, error, isLoading } = useGetAllProductsQuery();
  return (
    <div className="home-container" style={{position:"absolute",top:"120px",width:"100%"}}>
      {isLoading ? (
        <p>Loading...</p>
      ) : error ? (
        <p>An error occured</p>
      ) : (
        <>
          <h2>Product list</h2>
          <div className="products">
            {data.map((product) => (
              <div key={product.id} className="product">
                <Link to="/productitem">
                <h3>{product.name}</h3>
                <img src={product.image} alt={product.name} />
                </Link>
                
                <div className="details">
                  <span>{product.desc}</span>
                  <span className="price"><BiRupee/>{product.price}</span>
                </div>
                <button onClick={()=>handleAddToCart(product)}>Add to cart</button>
              </div>
            ))}
          </div>
        </>
      )}
      <Footer/>
    </div>
  );
};

export default Home;
