const express = require("express");
const cors = require("cors");
const products = require("./products");
const register = require("./routes/register");
const login = require("./routes/login");
const stripe = require("./routes/stripe");

const mongoose = require("mongoose");

require("dotenv").config();

const app = express();

app.use(express.json());
app.use(cors());

app.use("/api/register", register);
app.use("/api/login", login);
app.use("/api/stripe", stripe);

app.get("/", (req, res) => {
  res.send("Welcome to ecommerce website......");
});

app.get("/products", (req, res) => {
  res.send(products);
});

const port = process.env.PORT || 5000;

mongoose
  .connect(
    "mongodb+srv://vijaynatte1:9347470719@cluster0.qnclkgx.mongodb.net/?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => console.log("db connection established successfully....."))
  .catch((error) => console.log("connection failed", error.message));

app.listen(port, console.log(`server is running under ${port}`));
